# Keyterator
Iterate over your passwords and keys to improve your memory.

## How does it works?
Keyterator is an endless Iterating Bash script that helps you type and practice your password over and over. When your password is read, your typing is hidden and then send to /dev/null, it is not stored.
